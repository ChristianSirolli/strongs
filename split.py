import json
strong = open('/builds/ChristianSirolli/strongs/strong.json', 'r')
db=json.load(strong)
strong.close()
for k, v in db.items():
    f = open('/builds/ChristianSirolli/strongs/strong/{}.json'.format(k), 'w')
    v['title'] = int(k[1:])
    f.write(json.dumps(v))
    f.close()
