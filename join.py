import json
from pathlib import Path
from os import walk
Path("./public/strong").mkdir(parents=True, exist_ok=True)
for lang in ('hebrew', 'greek'):
    strong = open('./public/strong/{}strong.json'.format(lang[:1]), 'w')
    files = []
    content = {}
    for (dirpath, dirnames, filenames) in walk('./public/' + lang):
        files.extend(filenames)
        break
    for filename in files:
        f = open('./public/' + lang + '/' + filename)
        content[filename.split('.')[0]] = json.load(f)
        f.close()
    strong.write(json.dumps(content))
    strong.close()
